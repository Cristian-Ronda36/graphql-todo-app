import React, {useState} from 'react';
import {
  Platform,
  StatusBar,
  StyleSheet,
  View,
  SafeAreaView,
} from 'react-native';
import AppNavigator from './src/navigation/AppNavigator';
import CenterSpinner from './src/screens/components/Util/CenterSpinner';
import {NavigationContainer} from '@react-navigation/native';

const App = () => {
  const [isLoadingComplete] = useState(true);
  if (!isLoadingComplete) {
    return <CenterSpinner />;
  }
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <NavigationContainer>
          <AppNavigator />
        </NavigationContainer>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default App;
