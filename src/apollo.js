import {ApolloClient, HttpLink, InMemoryCache, split} from '@apollo/client';
import {WebSocketLink} from '@apollo/client/link/ws';
import {getMainDefinition} from '@apollo/client/utilities';

// Here can split HttpLink with WebSocketLink to diffenciate
// between both
// docs: https://www.apollographql.com/docs/react/data/subscriptions/#3-split-communication-by-operation-recommended
const httpLink = token =>
  new HttpLink({
    uri: 'https://hasura.io/learn/graphql',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

const wsLink = token =>
  new WebSocketLink({
    uri: 'wss://hasura.io/learn/graphql',
    options: {
      reconnect: true,
      connectionParams: {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    },
  });

const splitLink = token =>
  split(
    ({query}) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      );
    },
    wsLink(token),
    httpLink(token),
  );

const makeApolloClient = token => {
  // create an apollo link instance, a network interface for apollo client
  const link = splitLink(token);

  // create an inmemory cache instance for caching graphql data
  const cache = new InMemoryCache({
    typePolicies: {
      // field on data object
      online_users: {
        // name of query or subscription
        online_users: {
          merge: (existing = [], incoming) => {
            return incoming;
          },
        },
      },
    },
  });

  // instantiate apollo client with apollo link instance and cache instance
  const client = new ApolloClient({
    link,
    cache,
  });

  return client;
};

export default makeApolloClient;
