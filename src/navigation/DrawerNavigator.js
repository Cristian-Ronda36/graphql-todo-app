import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {Text, View} from 'react-native';
import OnlineUsers from '../screens/UsersScreen';
import LogoutScreen from '../screens/LogoutScreen';
import TodosTabs from './TodosTabNavigator';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

const UsersStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="UsersStack"
      component={OnlineUsers}
      options={{title: 'Online Users'}}
    />
  </Stack.Navigator>
);

function Feed() {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Feed Screen</Text>
    </View>
  );
}
function Article() {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Article Screen</Text>
    </View>
  );
}

const MyDrawer = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        drawerActiveTintColor: '#39235A',
        drawerInactiveTintColor: 'black',
        drawerLabelStyle: {
          fontSize: 15,
          marginLeft: 10,
        },
      }}>
      <Drawer.Screen name="Todos" component={TodosTabs} />
      <Drawer.Screen name="Users" component={UsersStack} />
      <Drawer.Screen name="Logout" component={LogoutScreen} />
    </Drawer.Navigator>
  );
};

export default MyDrawer;
