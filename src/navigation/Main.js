import React, {useCallback, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Drawer from './DrawerNavigator';
import CenterSpinner from '../screens/components/Util/CenterSpinner';
import {ApolloProvider} from '@apollo/client';
import makeApolloClient from '../apollo';
import {EMIT_ONLINE_EVENT} from '../services/todos/susbcriptions';
const Main = () => {
  const [client, setClient] = useState(null);

  const fetchSession = useCallback(async () => {
    // fetch session
    const session = await AsyncStorage.getItem('@todo-graphql:session');
    const sessionObj = JSON.parse(session);
    const {token, id} = sessionObj;
    const _client = makeApolloClient(token);
    setClient(_client);
    // update online state on backend
    setInterval(async () => {
      if (id) {
        await _client?.mutate({
          mutation: EMIT_ONLINE_EVENT,
          variables: {
            userId: id,
          },
        });
      }
    }, 35000);
  }, []);

  useEffect(() => {
    fetchSession();
  }, [fetchSession]);

  if (!client) {
    return <CenterSpinner />;
  }
  return (
    <ApolloProvider client={client}>
      <Drawer />
    </ApolloProvider>
  );
};

export default Main;
