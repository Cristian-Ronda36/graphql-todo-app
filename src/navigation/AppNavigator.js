import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import AuthScreen from '../screens/AuthScreen';
import App from './Main';

const Stack = createNativeStackNavigator();

const StackNavigation = () => (
  <Stack.Navigator
    initialRouteName="Loading"
    screenOptions={{header: () => <></>}}>
    <Stack.Screen name="Auth" component={AuthScreen} />
    <Stack.Screen name="Loading" component={AuthLoadingScreen} />
    <Stack.Screen name="Main" component={App} />
  </Stack.Navigator>
);

export default StackNavigation;
