import React from 'react';
import TabBarIcon from './components/TabBarIcon';
import PrivateTodos from '../screens/PrivateTodosScreen';
import PublicTodos from '../screens/PublicTodosScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

const MyTabs = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="PrivateTodos"
        component={PrivateTodos}
        options={{
          tabBarLabel: 'Private Todos',
          tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused} name="lock-outline" />
          ),
          tabBarOptions: {
            activeTintColor: '#392F76',
            inactiveTitColor: 'gray',
          },
        }}
      />
      <Tab.Screen
        name="PublicTodos"
        component={PublicTodos}
        options={{
          tabBarLabel: 'Public Todos',
          tabBarIcon: ({focused}) => (
            <TabBarIcon focused={focused} name="public" />
          ),
          tabBarOptions: {
            activeTintColor: '#392F76',
            inactiveTitColor: 'gray',
          },
        }}
      />
    </Tab.Navigator>
  );
};
export default MyTabs;
