import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

const TabBarIcon = ({name}) => {
  return <Icon name={name} size={26} style={{marginBottom: -3}} />;
};

export default TabBarIcon;
