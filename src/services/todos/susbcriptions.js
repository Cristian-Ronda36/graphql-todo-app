import {gql} from '@apollo/client';

// GraphQL mutation to update last_seen
export const EMIT_ONLINE_EVENT = gql`
  mutation {
    update_users(_set: {last_seen: "now()"}, where: {}) {
      affected_rows
    }
  }
`;

export const SUBSCRIBE_TO_ONLINE_USERS = gql`
  subscription {
    online_users(order_by: {user: {name: asc}}) {
      user {
        name
        id
      }
      id
    }
  }
`;

export const SUBSCRIBE_TO_NEW_TODOS = gql`
  subscription {
    todos(
      order_by: {created_at: desc}
      limit: 1
      where: {is_public: {_eq: true}}
    ) {
      id
      created_at
    }
  }
`;
