import React, {useEffect, useCallback} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import CenterSpinner from './components/Util/CenterSpinner';
import {logout} from '../authActions';
import {View} from 'react-native';
import {withApollo} from '@apollo/client/react/hoc';

const LogoutScreen = ({client}) => {
  const _logout = useCallback(() => {
    client.resetStore();
    AsyncStorage.removeItem('@todo-graphql:session').then(() => {
      logout();
    });
  }, [client]);

  useEffect(() => {
    _logout();
  }, [_logout]);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <CenterSpinner />
    </View>
  );
};

LogoutScreen.navigationOptions = {
  drawerLabel: 'Logout',
  title: 'Logging out',
};

export default withApollo(LogoutScreen);
