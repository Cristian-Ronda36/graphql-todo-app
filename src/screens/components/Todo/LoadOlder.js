import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import CenterSpinner from '../Util/CenterSpinner';
import {FETCH_OLD_TODOS, FETCH_TODOS} from '../../../services/todos/queries';
import {withApollo} from '@apollo/client/react/hoc';

const LoadOlderButton = ({styles, isPublic, client}) => {
  const [buttonText, setButtonText] = React.useState('Load more todos');
  const [loading, setLoading] = React.useState(false);
  const [disabled, setDisabled] = React.useState(false);
  const fetchOlderTodos = async () => {
    // Read from device's cache
    const data = client.readQuery({
      query: FETCH_TODOS,
      variables: {
        isPublic,
      },
    });
    const numTodos = data.todos.length;
    setDisabled(true);
    setLoading(true);
    // Do a request to server to get the old todos
    const response = await client.query({
      query: FETCH_OLD_TODOS,
      variables: {
        isPublic,
        lastId: numTodos === 0 ? 0 : data.todos[numTodos - 1].id,
      },
    });
    setLoading(false);
    if (!response.data) {
      setDisabled(false);
      return;
    }
    if (response.data.todos) {
      // update device's cache
      client.writeQuery({
        query: FETCH_TODOS,
        variables: {
          isPublic,
        },
        data: {todos: [...data.todos, ...response.data.todos]},
      });
      if (response.data.todos.length < 10) {
        setButtonText('No more todos');
      } else {
        setButtonText('Load more todos');
      }
      setDisabled(false);
    } else {
      setButtonText('Load more todos');
    }
  };

  return (
    <TouchableOpacity
      style={styles.pagination}
      disabled={disabled}
      onPress={fetchOlderTodos}>
      {loading ? (
        <CenterSpinner />
      ) : (
        <Text style={styles.buttonText}>{buttonText}</Text>
      )}
    </TouchableOpacity>
  );
};
// Recommendation use graphql because has more features
// doc: https://www.apollographql.com/docs/react/api/react/hoc/#withapollocomponent
export default withApollo(LoadOlderButton);
