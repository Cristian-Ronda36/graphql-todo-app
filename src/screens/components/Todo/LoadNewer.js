import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import CenterSpinner from '../Util/CenterSpinner';
import {FETCH_NEW_TODOS, FETCH_TODOS} from '../../../services/todos/queries';
import {withApollo} from '@apollo/client/react/hoc';

const LoadNewerButton = ({show, styles, isPublic, client, ...props}) => {
  console.log(props);
  const [buttonText, setButtonText] = React.useState('New todos have arrived');
  const [loading, setLoading] = React.useState(false);
  const fetchNewerTodos = async () => {
    // Here read todo list from cache
    const data = client.readQuery({
      query: FETCH_TODOS,
      variables: {
        isPublic,
      },
    });
    // check last item in list
    const lastId = data.todos[0].id;
    setLoading(true);
    // fetch new todos from backend
    const resp = await client.query({
      query: FETCH_NEW_TODOS,
      variables: {lastId},
    });
    setLoading(false);
    // auto merge here you can update in apollo.js
    // in InMemoryCache
    if (resp.data) {
      const newData = {
        todos: [...resp.data.todos, ...data.todos],
      };
      // update cache
      client.writeQuery({
        query: FETCH_TODOS,
        variables: {
          isPublic,
        },
        data: newData,
      });
      props.toggleShow();
    }
  };
  if (!show) {
    return null;
  }

  return (
    <TouchableOpacity
      style={styles.banner}
      disabled={loading}
      onPress={fetchNewerTodos}>
      {loading ? (
        <CenterSpinner />
      ) : (
        <Text style={styles.buttonText}>{buttonText}</Text>
      )}
    </TouchableOpacity>
  );
};

export default withApollo(LoadNewerButton);
