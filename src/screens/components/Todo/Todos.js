import React, {useCallback, useEffect} from 'react';
import {ScrollView, StyleSheet, View, FlatList, Text} from 'react-native';
import TodoItem from './TodoItem';
import LoadOlder from './LoadOlder';
import LoadNewer from './LoadNewer';
import CenterSpinner from '../Util/CenterSpinner';
import {useQuery} from '@apollo/client';
import {FETCH_TODOS} from '../../../services/todos/queries';
import {withApollo} from '@apollo/client/react/hoc';
import {SUBSCRIBE_TO_NEW_TODOS} from '../../../services/todos/susbcriptions';

// Publick todos
const Todos = ({isPublic, client, ...props}) => {
  const [newTodosExist, setNewTodosExist] = React.useState(false);

  const subscribeToNewTodos = useCallback(() => {
    if (isPublic) {
      // subscription to new TODOS
      client
        .subscribe({
          query: SUBSCRIBE_TO_NEW_TODOS,
        })
        .subscribe({
          next: event => {
            if (event.data.todos.length) {
              let localData;
              try {
                // Read device's cache
                localData = client.readQuery({
                  query: FETCH_TODOS,
                  variables: {
                    isPublic: true,
                  },
                });
              } catch (e) {
                return;
              }
              // get last todo to verify if exist todo to fetch more
              const lastId = localData?.todos[0] ? localData.todos[0]?.id : 0;
              if (event.data.todos[0].id > lastId) {
                setNewTodosExist(true);
              }
            }
          },
          error: err => {
            console.error('err', err);
          },
        });
    }
  }, [client, isPublic]);

  useEffect(() => {
    subscribeToNewTodos();
  }, [subscribeToNewTodos]);

  const {data, error, loading} = useQuery(FETCH_TODOS, {
    variables: {isPublic},
  });

  if (error) {
    console.error(error);
    return <Text>Error</Text>;
  }

  if (loading) {
    return <CenterSpinner />;
  }
  const dismissNewTodoBanner = () => {
    setNewTodosExist(false);
  };
  return (
    <View style={styles.container}>
      <LoadNewer
        show={newTodosExist && isPublic}
        styles={styles}
        isPublic={isPublic}
        toggleShow={dismissNewTodoBanner}
      />
      <View style={styles.scrollView}>
        <FlatList
          data={data.todos}
          renderItem={({item}) => <TodoItem item={item} isPublic={isPublic} />}
          keyExtractor={item => item.id.toString()}
        />
        <LoadOlder isPublic={isPublic} styles={styles} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 0.8,
    paddingHorizontal: 10,
    backgroundColor: '#F7F7F7',
  },
  scrollViewContainer: {
    justifyContent: 'flex-start',
  },
  banner: {
    flexDirection: 'column',
    backgroundColor: '#39235A',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
  },
  pagination: {
    flexDirection: 'row',
    backgroundColor: '#39235A',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    borderRadius: 5,
    marginBottom: 20,
    paddingVertical: 5,
  },
  buttonText: {
    color: 'white',
  },
});

export default withApollo(Todos);
